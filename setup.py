#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name             = "vispa_pscan",
    version          = "0.1.0",
    author           = "VISPA Project",
    author_email     = "vispa@lists.rwth-aachen.de",
    description      = "VISPA Parameter Scan View Extension",
    url              = "http://vispa.physik.rwth-aachen.de/",
    license          = "GNU GPL v2",
    package_dir      = {"vispa_pscan": "vispa_pscan"},
    package_data     = {"vispa_pscan": [
        "workspace/*",
        "static/css/*",
        "static/js/*",
        "static/html/*",
        "static/img/*"
    ]}
)
