define([
  "jquery",
  "vispa/extension",
  "vispa/views/main",
  "vispa/filehandler2",
  "./pscan",
  "css!../css/styles"
], function($, Extension, MainView, FileHandler2, PScan) {

  var PScanExtension = Extension._extend({

    init: function init() {
      init._super.call(this, "pscan", "Parameter Scanner");
      this.mainMenuAdd([
        this.addView(PScanView),
      ]);
    }
  });


  var PScanView = MainView._extend({

    init: function init(args) {
      init._super.apply(this, arguments);

      this.pscan = new PScan(this);

      this.state.setup({
        directory: undefined,
        regExp: undefined
      }, args);

      this.on("stateChange", function(key) {
        if (~["directory", "regexp"].indexOf(key)) {
          var $elem = this.pscan.nodes["$" + key]
          if ($elem) {
            $elem.val(this.state.get(key));
          }
        }
      }.bind(this));
    },

    render: function($node) {
      this.pscan.render($node);
    },

    applyPreferences: function applyPreferences() {
      applyPreferences._super.call(this);

      this.pscan.setWidth(this.prefs.get("widthFraction"));
      this.pscan.setRegExpBookmarks(this.prefs.get("regexpBookmarks"));
    },

    getFragment: function() {
      return (this.state.get("directory") || "") + ":" +
             (this.state.get("regexp")    || "");
    },

    applyFragment: function(fragment) {
      var parts = fragment.split(":");
      if (parts.length >= 1) {
        this.state.set("directory", parts[0]);
      }
      if (parts.length >= 2) {
        this.state.set("regexp", parts[1]);
      }
    },
  },{
    label: "PScan",
    iconClass: "fa-list-ol",
    name: "pscan",
    menuPosition: 100,
    preferences: {
      items: {
        widthFraction: {
          level: 3,
          label: "Width ratio %",
          type : "integer",
          value: 55,
          range: [5, 95, 1],
          // description: "Width fraction in %."
        },
        selectViaArrows: {
          level: 2,
          label: "Arrow navigation",
          type : "boolean",
          value: false,
          description: "Select files in the overview table via arrows."
        },
        regexpBookmarks: {
          // type: "list",
          value: [],
          level: 4,
        },
      },
    },
    menu: {
      scrolltop: {
        label: "Scroll to top",
        iconClass: "fa fa-hand-o-up",
        buttonClass: "btn-default",
        callback: function() {
          if (this.$root.instance.pscan.nodes.$controls) {
            this.$root.instance.pscan.nodes.$controls.find(".controls-wrapper").scrollTop(0);
          }
        }
      },
      values: {
        label: "Show values",
        iconClass: "fa fa-sliders",
        buttonClass: "btn-default",
        callback: function() {
          if (this.$root.instance.pscan.nodes.$controls) {
            this.$root.instance.pscan.nodes.$controls.find("#tabs a#manipulators-link").tab("show");
          }
        }
      },
      files: {
        label: "Show files",
        iconClass: "fa fa-table",
        buttonClass: "btn-default",
        callback: function() {
          if (this.$root.instance.pscan.nodes.$controls) {
            this.$root.instance.pscan.nodes.$controls.find("#tabs a#overview-link").tab("show");
          }
        }
      },
    },
  });

  FileHandler2.addOpen("pscan", {
    label: "PScan",
    iconClass: "fa-list-ol",
    position: 100,
  }, function(path) {
      return ["pscan", "pscan", {directory: path}];
  }, false);

  return PScanExtension;
});
