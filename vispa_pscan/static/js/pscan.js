define([
  "jquery",
  "emitter",
  "vispa/utils",
  "text!../html/main.html",
  "text!../html/frame.html",
  "text!../html/bookmark.html",
  "text!../html/manipulator.html",
], function(
  $,
  Emitter,
  Utils,
  MainTemplate,
  FrameTemplate,
  BookmarkTemplate,
  ManipulatorTemplate
) {

  var PScan = Emitter._extend({

    init: function init(view) {
      init._super.call(this);

      this.view = view;

      this.indexes = [];
      this.cache   = null;

      this.manipulatorTypes = [];

      this.$currentFrame = null;

      this.nodes = {
        $main    : null,
        $controls: null,
        $image   : null
      };

      this.syncingManipulators = false;
    },

    render: function($node) {
      var self = this;

      this.view.loading = true;

      var $main     = $(MainTemplate).appendTo($node);
      var $controls = $main.find(".controls-resize-wrapper");
      var $image    = $main.find(".image-wrapper");

      $controls.resizable({
        handles: "e",
        start: function() {
          var mainWidth  = $main.width();
          $controls.resizable("option", "grid", [mainWidth * 0.01, 1]);
        },
        resize: function() {
          var mainWidth     = $main.width();
          var controlsWidth = $controls.width();
          $controls.css({
            left : 0,
            width: controlsWidth
          });
          $image.css({
            left : controlsWidth,
            width: mainWidth - controlsWidth
          });

          self.onWidthChange();
        },
        stop: function() {
          var controlsWidth = $controls.width();
          var mainWidth     = $main.width();
          var frac = Math.round(100.0 * controlsWidth / mainWidth);
          frac = Math.max(5, Math.min(95, frac));

          // tell the preferences
          self.view.prefs.set("widthFraction", frac, true);
        }
      });

      var $directory = $controls.find("#dir");

      var $selectBtn = $controls.find("#select-dir").click(function($event) {
        var args = {
          callback: function(directory) {
            $directory.val(directory);
            self.load();
          },
          foldermode: true,
          path: $directory.val() || "$HOME"
        };
        self.view.spawnInstance("file2", "FileSelector", args);
      });

      $controls.find("#goto-dir").click(function($event) {
        var directory = $directory.val();
        if (directory) {
          var args = { path: directory };
          self.view.spawnInstance("file2", "FileMain", args);
        }
      });

      $controls.find("#scan").click(function($event) {
        self.load();
      });

      $controls.find("#tabs").on("shown.bs.tab", function($event) {
        self.updateView();
      });

      var $regexpBookmarks = $controls.find("#regexp-bookmarks");
      $regexpBookmarks.find("#add-regexp-bookmark").click(function($event) {
        $event.preventDefault();

        var value = self.nodes.$regexp.val();
        if (!value) {
          return;
        }

        self.view.prompt("Name your RegExp:", function(name) {
          if (name) {
            var prefs = self.view.prefs.get("regexpBookmarks");
            var exists = false;
            for (var i = 0; i < prefs.length; ++i) {
              if (prefs[i].name == name) {
                exists = true;
                break;
              }
            }
            if (!exists) {
              var pref = {name: name, value: value};
              prefs.push(pref);
              self.setRegExpBookmarks(prefs);
            } else {
              self.view.alert("A bookmark named '" + name + "' does already exist.");
            }
          }
        });
      });

      self.nodes.$main             = $main;
      self.nodes.$controls         = $controls;
      self.nodes.$image            = $image;
      self.nodes.$directory        = $directory;
      self.nodes.$selectBtn        = $selectBtn;
      self.nodes.$regexp           = $controls.find("#regexp");
      self.nodes.$nFilesBadge      = $controls.find("#n-files");
      self.nodes.$scanIcon         = $controls.find("#scan > i");
      self.nodes.$overviewTab      = $controls.find("#overview");
      self.nodes.$manipulatorsTab  = $controls.find("#manipulators");
      self.nodes.$placeholderText  = $main.find("#placeholder span");
      self.nodes.$regexpBookmarks  = $regexpBookmarks;

      self.view.applyPreferences();
      self.view.loading = false;

      // set initial values
      self.nodes.$directory.val(self.view.state.get("directory") || "");
      self.nodes.$regexp.val(   self.view.state.get("regexp")    || "");

      // scan when input fields are set
      if (self.view.state.get("directory") && self.view.state.get("regexp")) {
        self.load();
      }
    },

    setWidth: function(widthFraction) {
      if (!this.nodes.$main) return;

      this.nodes.$controls.removeAttr("style").css({
        left : 0,
        width: widthFraction + "%"
      });
      this.nodes.$image.removeAttr("style").css({
        right: 0,
        width: (100 - widthFraction) + "%"
      });

      this.onWidthChange();
    },

    load: function() {
      var self = this;

      if (!this.nodes.$main) {
        return this;
      }

      var req = {};
      var failedKey = null;
      ["directory", "regexp"].forEach(function(key) {
        var value = self.nodes["$" + key].val();
        if (!failedKey) {
          failedKey = value ? failedKey : key;
          req[key]  = value;
        }
      });
      if (failedKey) {
        this.nodes["$" + failedKey].focus();
        if (failedKey == "directory") {
          this.view.setLabel("PScan");
        }
        return this;
      }

      this.view.loading = true;
      this.nodes.$scanIcon.attr("class", "fa fa-spinner fa-spin");

      this.view.GET("files", req, function(err, files) {
        if (err) return;

        self.view.setLabel(req.directory, true);

        if (!self.cache || self.cache.directory != req.directory) {
          if (self.cache) {
            self.cache.deleteRecursive();
            delete self.cache;
          }
          self.cache = new PScanCache(req.directory);
        }

        var oldLabels = self.cache.labels;
        self.cache.loadFiles(files);
        var newLabels = self.cache.labels;
        if (!self.cache.arraysEqual(oldLabels, newLabels)) {
          self.manipulatorTypes.length = 0;
        }
        self.updateView();

        self.view.loading = false;
        self.nodes.$scanIcon.attr("class", "fa fa-play");

        self.view.state.set("directory", req.directory);
        self.view.state.set("regexp", req.regexp);
      });

      return this;
    },

    updateView: function() {
      var self = this;

      if (!this.cache) {
        return this;
      }

      // update labels and button states
      this.nodes.$nFilesBadge.html(this.cache.length);

      // update the files table
      var labels = this.cache.labels.map(function(label) {
        return { label: label };
      });
      var rows       = [];
      var rowsValues = [];
      var axes = this.cache.labels.map(function(label) {
        return self.cache.axes[label];
      });
      this.cache.iterProduct(axes).forEach(function(values) {
        var obj = self.cache.get(values);
        if (!obj) {
          return;
        }
        rowsValues.push(values);
        rows.push({
          row: values.map(function(value) { return { value: value }; })
        });
      });
      var $headRow = this.nodes.$overviewTab.find("thead > tr");
      var $body    = this.nodes.$overviewTab.find("tbody");
      $headRow.render(labels);
      $body.render(rows);
      // mouse and key events
      $.makeArray($body.find("tr")).forEach(function(tr, i) {
        var $tr = $(tr).attr("data-values", rowsValues[i].join(","));
        var onClick = function($event) {
          self.onValueChange(rowsValues[i]);
        };
        $tr.click(onClick).keydown(function($event) {
          if (~[13, 37, 39].indexOf($event.which)) {
            // enter, arrow right, arrow right
            $tr.trigger("click");
          } else if ($event.which == 38) {
            // arrow up
            if (self.view.prefs.get("selectViaArrows")) {
              $tr.prev().trigger("click");
            } else {
              $tr.prev().focus();
            }
          } else if ($event.which == 40) {
            // arrow down
            if (self.view.prefs.get("selectViaArrows")) {
              $tr.next().trigger("click");
            } else {
              $tr.next().focus();
            }
          } else if ($event.which == 8 || $event.which == 46) {
            // backspace or remove
            // no action yet
          } else {
            return;
          }
          $event.stopPropagation();
          $event.preventDefault();
        });
      });

      // update the manipulators
      var $manipulatorsBody = this.nodes.$manipulatorsTab.find("tbody");
      $manipulatorsBody.empty();

      self.cache.labels.forEach(function(label, i) {
        var axis = self.cache.axes[label];

        var index = i < self.indexes.length ? self.indexes[i] : 0;
        var value = axis[index];

        var $manipulator = $(ManipulatorTemplate)
          .render({ label: label })
          .attr("data-label-index", i)
          .appendTo($manipulatorsBody);

        var $readonly = $manipulator.find("#readonly");
        var $slider   = $manipulator.find("#slider");
        var $dropdown = $manipulator.find("#dropdown");

        // readonly
        $readonly.val(value);

        // slider
        $slider.css("width", 80 * (axis.length - 1)).slider({
          min         : 0,
          max         : axis.length - 1,
          value       : index,
          tooltip     : "hide",
          focus       : true,
          ticks       : axis.map(function(_, j) { return j; }),
          ticks_labels: axis.map(function(j) { return j;})
        }).on("change", function() {
          if (!self.syncingManipulators) {
            var idx = $slider.slider("getValue");
            $manipulator.data("index", idx);
            self.onValueChange(undefined);
          }
        });

        // dropdown
        var options = axis.map(function(step, j) {
          return $("<option>").html(step).attr("value", j);
        });
        $dropdown.append(options).selectpicker({
          size: 6
        }).selectpicker("val", index).change(function() {
          if (!self.syncingManipulators) {
            var idx = parseInt($dropdown.selectpicker("val"));
            $manipulator.data("index", idx);
            self.onValueChange(undefined);
          }
        });

        // set index and value
        $manipulator.data("index", index);

        // type menu
        var $typeMenu = $manipulator.find("#manipulator-menu > ul");
        var setType = function(type) {
          $typeMenu.find("a > i.pull-right").hide();
          $typeMenu.find("a#type-" + type + " > i.pull-right").show();
          self.manipulatorTypes[i] = type;
          self.onWidthChange();
        };
        setType(self.manipulatorTypes[i] || "auto");
        ["auto", "slider", "dropdown"].forEach(function(type) {
          $typeMenu.find("a#type-" + type).click(function($event) {
            $event.preventDefault();
            setType(type);
          });
        });
      });

      self.onWidthChange();
      self.onValueChange();

      // toggle table display
      var spanDisplay  = this.cache.length ? "none" : "block";
      var tableDisplay = this.cache.length ? "table" : "none";
      this.nodes.$overviewTab.find("> span").css("display", spanDisplay);
      this.nodes.$manipulatorsTab.find("> span").css("display", spanDisplay);
      this.nodes.$overviewTab.find("table").css("display", tableDisplay);
      this.nodes.$manipulatorsTab.find("table").css("display", tableDisplay);
    },

    onWidthChange: function() {
      var self = this;

      if (!this.cache) {
        return this;
      }

      var $manipulators = this.nodes.$manipulatorsTab;

      var colWidth = this.nodes.$manipulatorsTab.find("#readonly").first().parent().width();

      this.cache.labels.forEach(function(label, i) {
        var axis = self.cache.axes[label];

        var $manipulator = $manipulators.find(".manipulator[data-label-index='" + i + "']");
        var $readonly    = $manipulator.find("#readonly");
        var $slider      = $manipulator.find("#slider");
        var $dropdown    = $manipulator.find("#dropdown");

        var manType = self.manipulatorTypes[i];

        if (axis.length == 1) {
          $readonly.show();
          $slider.prev().hide();
          $dropdown.next().hide();
        } else if (manType == "slider" || (manType == "auto" && ((colWidth - 70) / (axis.length - 1)) > 80)) {
          $readonly.hide();
          $slider.prev().show();
          $dropdown.next().hide();
        } else {
          $readonly.hide();
          $slider.prev().hide();
          $dropdown.next().show();
        }
      });

      // relayout all sliders
      $manipulators.find(".manipulator #slider").slider("relayout");

      return this;
    },

    onValueChange: function(values) {
      var self = this;

      if (this.syncingManipulators) {
        return this;
      }
      this.syncingManipulators = true;

      var $manipulators = this.nodes.$manipulatorsTab;

      this.indexes.length = 0;

      var indexes = undefined;
      if (values !== undefined) {
        indexes = this.cache.getIndexes(values);
      }

      $.makeArray($manipulators.find(".manipulator")).forEach(function(manipulator, i, array) {
        var $manipulator = $(manipulator);
        var $readonly    = $manipulator.find("#readonly");
        var $slider      = $manipulator.find("#slider");
        var $dropdown    = $manipulator.find("#dropdown");

        var index, value;
        if (indexes) {
          index = indexes[i];
          value = values[i];
        } else {
          index = $(manipulator).data("index");
          value = self.cache.axes[self.cache.labels[i]][index];
        }

        $(manipulator).data("index", index);
        $readonly.val(value);
        $slider.slider("setValue", index);
        $dropdown.selectpicker("val", index);

        self.indexes.push(index);
      });

      if (!this.indexes.length) {
        return this;
      }

      this.syncingManipulators = false;

      values = values || this.cache.getValues(this.indexes);

      // highlight the specific row in the overview table
      this.nodes.$overviewTab.find("tbody > tr.success").toggleClass("success", false);
      var trSelector = "tbody > tr[data-values='" + values.join(",") + "']";
      var $tr = this.nodes.$overviewTab.find(trSelector).toggleClass("success", true)
      if (this.nodes.$overviewTab.hasClass("active")) {
        $tr.focus();
      }

      // load the frame if not already cached
      var obj = this.cache.get(values);

      // if obj is null, show the error
      if (!obj) {
        if (self.$currentFrame) {
          self.$currentFrame.hide();
        }
        self.nodes.$placeholderText.html("Cannot find image for values:<br />" + values.join(", "));
      } else {
        this.loadFrame(obj, function(err) {
          if (self.$currentFrame) {
            self.$currentFrame.hide();
          }
          obj.$node.show();
          self.$currentFrame = obj.$node;
        });
        self.nodes.$placeholderText.html("Scan to load images");
      }

      // determine values that are available from the current values
      var availableIndexes = [];
      values.forEach(function(value, i) {
        var axis = self.cache.axes[self.cache.labels[i]];
        var _indexes = [];
        for (var j = 0; j < axis.length; ++j) {
          var valuesCandidate = values.concat();
          valuesCandidate[i] = axis[j];
          if (self.cache.get(valuesCandidate)) {
            _indexes.push(j);
          }
        }
        availableIndexes.push(_indexes);
      });

      // update color of entries in manipulators depending on availability
      $.makeArray($manipulators.find(".manipulator")).forEach(function(manipulator, i) {
        var $manipulator = $(manipulator);
        var $slider      = $manipulator.find("#slider");
        var $dropdown    = $manipulator.find("#dropdown");

        var checkColor = function(j, elem) {
          var color = ~availableIndexes[i].indexOf(j) ? "#333" : "#c9302c";
          $(elem).css("color", color);
        };

        $slider.prev().find(".slider-tick-label").each(checkColor);
        $dropdown.next().find("span.text").each(checkColor);
        checkColor(self.indexes[i], $dropdown.next().find("> button > .filter-option"));
      });

      return this;
    },

    loadFrame: function(fileObj, callback) {
      var self = this;

      callback = callback || function(){};

      if (fileObj.$node) {
        callback(null);
        return;
      }

      fileObj.$node = $(FrameTemplate).render(fileObj).appendTo(self.nodes.$image);

      fileObj.$node.find("iframe").attr("src", self.view.fileURL(fileObj.filePath));

      fileObj.$node.find("#refresh-frame").click(function($event) {
        fileObj.$node.find("iframe")[0].contentDocument.location.reload(true);
      });

      callback(null);
    },

    setRegExpBookmarks: function(bookmarks) {
      var self = this;

      if (!this.nodes.$regexpBookmarks) {
        return this;
      }

      this.nodes.$regexpBookmarks.find(".bookmark-item").remove();

      bookmarks.concat().reverse().forEach(function(bookmark) {
        var $bookmark = $(BookmarkTemplate);
        var label = "<b>" + bookmark.name + "</b>: " + bookmark.value;
        $bookmark.find("a").html(label).click(function($event) {
          $event.preventDefault();
          self.nodes.$regexp.val(bookmark.value);
        }).attr("title", bookmark.value).tooltip({
          delay: { "show": 750, "hide": 0 },
          placement: "bottom"
        });

        $bookmark.find(".item-remove").click(function($event) {
          $event.stopPropagation();
          var prefs = self.view.prefs.get("regexpBookmarks");
          var idx = -1;
          for (var i = 0; i < prefs.length; ++i) {
            if (prefs[i].name == bookmark.name) {
              idx = i;
              break;
            }
          }
          if (~idx) {
            prefs.splice(idx, 1);
          }
          self.setRegExpBookmarks(prefs);
        }).keydown(function($event) {
          if ($event.which == 13) {
            $(this).trigger("click");
          }
        }).attr("title", "Remove item");

        $bookmark.prependTo(self.nodes.$regexpBookmarks);
      });

      return this;
    }
  });


  var PScanCache = Emitter._extend({

    init: function init(directory) {
      init._super.call(this);

      this.directory = directory;

      this.labels = [];
      this.axes   = {};
      this.matrix = {};

      this.files = [];

      this.length = 0;
    },

    get: function(values, create) {
      if (create === undefined) {
        create = false;
      }

      if (values === undefined) {
        values = [];
      }

      var data = this.matrix;
      values.forEach(function(value) {
        if (data === undefined) {
          return;
        }
        if (create && data[value] === undefined) {
          data[value] = {};
        }
        data = data[value];
      });

      return data;
    },

    getValues: function(indexes) {
      return indexes.map(function(index, i) {
        return this.axes[this.labels[i]][index];
      }, this);
    },

    getIndexes: function(values) {
      return values.map(function(value, i) {
        return this.axes[this.labels[i]].indexOf(value);
      }, this);
    },

    deleteRecursive: function(values) {
      var data = this.get(values);
      if (!data) {
        return this;
      }

      // walk trough the data until a file object is found
      // if so, remove its $node and decrement our length
      var lookup = [data];
      while (lookup.length) {
        var obj = lookup.shift();
        if (obj.mtime) {
          if (obj.$node) {
            obj.$node.remove();
          }
          this.length--;
        } else {
          for (var key in obj) {
            if (typeof(obj[key]) == "object") {
              lookup.push(obj[key]);
            }
          }
        }
      }

      Object.keys(data).forEach(function(key) {
        delete data[key];
      });

      return this;
    },

    loadFiles: function(files) {
      var self = this;

      // flush when the labels changed
      // therefore, compare with the labels of the first file
      var labels = !files.length ? [] : files[0].specs.map(function(spec) {
        return spec.label;
      });
      if (!this.arraysEqual(labels, this.labels)) {
        this.deleteRecursive([]);
        this.matrix = {};
        delete this.axes;
        this.axes = {};
        delete this.labels;
        this.labels = labels;
      }

      // parse axes
      var axes = {};
      labels.forEach(function(label) {
        var axis = axes[label] = [];
        files.forEach(function(file) {
          for (var i = 0; i < file.specs.length; ++i) {
            if (file.specs[i].label == label) {
              var value = file.specs[i].value;
              if (!~axis.indexOf(value)) {
                axis.push(value);
              }
              break
            }
          }
        });
        axis.sort().sort(function(a, b) { return a - b; });
      });

      // go through old axes and check if their values still exist
      labels.forEach(function(label) {
        if (!~Object.keys(self.axes).indexOf(label)) {
          return;
        }
        self.axes[label].forEach(function(value) {
          // value still present?
          if (~axes[label].indexOf(value)) {
            // yes, exit
            return;
          }
          // not present, delete each matrix element that contains
          // the value of the axis defined by label
          var iterables = labels.map(function(_label) {
            return _label == label ? [value] : self.axes[_label];
          });
          self.iterProduct(iterables).forEach(function(values) {
            self.deleteRecursive(values);
          });
        });
      });
      delete this.axes;
      this.axes = axes;

      // finally, add or update files
      files.forEach(function(newFile) {
        var values = newFile.specs.map(function(spec) {
          return spec.value;
        });
        var oldFile = self.get(values, true);
        if (!oldFile.mtime || oldFile.mtime != newFile.mtime) {
          // increment our length when there was no file object
          if (!oldFile.mtime) {
            self.length++;
          }
          // remove the old node
          if (oldFile.$node) {
            oldFile.$node.remove();
            delete oldFile.$node;
          }
          // update the file object
          $.extend(oldFile, newFile);
        }
      });

      this.files = files;

      return this;
    },

    arraysEqual: function(a1, a2) {
      if (a1.length != a2.length) {
        return false;
      }
      for (var i = 0; i < a1.length; ++i) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    },

    iterProduct: function(iterables) {
      return iterables.reduce(function(previous, current) {
        var combi = [];
        previous.forEach(function(a1) {
          current.forEach(function(a2) {
            combi.push(a1.concat(a2));
          });
        });
        return combi;
      }, [[]]);
    }
  });


  return PScan;
});
