# -*- coding: utf-8 -*-

import cherrypy
import vispa.workspace
from vispa.controller import AbstractController
from vispa.server import AbstractExtension
from vispa import AjaxException


class PScanExtension(AbstractExtension):

    def name(self):
        return "pscan"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(PScanController(self))
        self.add_workspace_directoy("workspace")


class PScanController(AbstractController):

    def __init__(self, extension):
        AbstractController.__init__(self)
        self.extension = extension

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def files(self, directory, regexp):
        self.release_session()
        rpc = self.extension.get_workspace_instance("PScanRpc")
        self.release_database()
        try:
            return rpc.files(directory, regexp)
        except Exception as e:
            raise AjaxException(e.message)
