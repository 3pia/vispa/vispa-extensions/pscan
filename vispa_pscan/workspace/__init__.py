# -*- coding: utf-8 -*-

import os
import logging
import re
import json
from time import strftime, localtime
from multiprocessing.dummy import Pool
from subprocess import Popen, PIPE

logger = logging.getLogger(__name__)
pool = Pool(5)


class PScanRpc:

    def __init__(self):
        logger.debug("PScanRpc created")

        self.mtime_format = "%Y/%m/%d %H:%M:%S"

    def files(self, directory, regexp):
        _directory = os.path.expandvars(os.path.expanduser(directory))
        if not os.path.isdir(_directory):
            raise Exception("directory not found: %s" % directory)
        directory = _directory

        cre = re.compile(regexp)
        nameIndex = dict(map(reversed, cre.groupindex.items()))
        labels = [
            nameIndex.get(i, "Groups %s" % i)
            for i in range(1, cre.groups + 1)
        ]

        vals = {}

        def prepare(match):
            abspath = os.path.join(directory, match.string)
            return match, abspath, os.stat(abspath).st_mtime

        for match, abspath, mtime in pool.map(
            prepare,
            filter(
                None,
                map(
                    cre.search,
                    self.list_files(directory)
                )
            )
        ):
            path = match.string

            values = tuple(map(cast, match.groups()))
            if values in vals:
                # vals[values] = None
                raise Exception("ambiguous matches for regexp")
            else:
                vals[values] = {
                    "fileName": path,
                    "filePath": abspath,
                    "mtime": mtime,
                    "mtimeString": strftime(self.mtime_format, localtime(mtime)),
                    "specs": [
                        {"label": l, "value": v}
                        for l, v in zip(labels, values)
                    ],
                }

        return json.dumps(filter(None, vals.values()))

    def list_files(self, base):
        # really fast
        try:
            return (
                l.strip()
                for l in Popen(
                    ["find", base, "-type", "f", "-printf", "%P\n"],
                    stdout=PIPE,
                    universal_newlines=True
                ).stdout
            )
        except OSError:
            pass
        # fallback
        return (
            os.path.relpath(os.path.join(dirpath, fn), base)
            for dirpath, dirnames, filenames in os.walk(base)
            for fn in filenames
        )


def cast(value):
    try:
        return int(value)
    except:
        pass
    try:
        return float(value)
    except:
        return value
